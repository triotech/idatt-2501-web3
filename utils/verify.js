const { run } = require("hardhat")

/**
 * Verifies a deployed contract on Etherscan.
 * This function uses Hardhat's Etherscan plugin to verify the contract source code,
 * allowing it to be readable and verified on Etherscan.
 *
 * @param {string} contractAddress - The deployed contract's address that needs to be verified.
 * @param {Array} args - The arguments that were used in the contract's constructor.
 * 
 * @async
 * @function verify
 */
const verify = async (contractAddress, args) => {
    console.log("Verifying contract...")
    try {
        await run("verify:verify", {
            address: contractAddress,
            constructorArguments: args,
        })
    } catch (error) {
        if (error.message.toLowerCase().includes("already verified")) {
            console.log("Already verified!")
        } else {
            console.log(error)
        }
    }
}

module.exports = {
    verify,
}
