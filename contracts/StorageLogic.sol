// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

/**
 * @title Storage Logic Contract
 * @dev Manages user registration, deposits, withdrawals, and balance updates.
 *      Utilizes a minimal deposit and draw system for managing user funds.
 */
contract StorageLogic {

    //Modifier to restrict certain functions to the contract owner only
    modifier onlyOwner() {
        require(msg.sender == i_owner, "Storage__NotOwner");
        _;
    }

    // Immutable variables set at contract deployment.
    uint256 private immutable i_minDeposit;
    uint256 private immutable i_minDraw;
    address payable private immutable i_owner;
    
    // Constant representing the ratio of tokens to ETH.
    uint256 private constant TOKEN_ETH_RATIO = 1e14;

    // Array to store registered users.
    string[] private s_users;
    // Mapping from user ids to their token balances.
    mapping (string => uint256) private s_tokens;

    /**
     * @dev Constructor sets the initial minimum deposit and draw values, and initializes the owner.
     * @param _minDeposit The minimum amount required for deposit.
     * @param _minDraw The minimum amount that can be withdrawn.
     */
    constructor(uint256 _minDeposit, uint256 _minDraw) {
        i_minDeposit = _minDeposit;
        i_minDraw = _minDraw;
        i_owner = payable (msg.sender);
        s_users = new string[](0);
        s_users.push("owner");
        s_tokens["owner"] = 0;
       
    }

    /**
     * @dev Allows users to deposit ETH into the contract.
     * @param _user The username to which the deposit is credited.
     */
    function deposit(string memory _user) public payable virtual {
        require(msg.value >= i_minDeposit, "Storage__NotEnoughTokens");
        s_tokens[_user] = s_tokens[_user] + msg.value;
    }

    /**
     * @dev Allows users to withdraw ETH from the contract.
     * @param _user The username from which the draw is made.
     * @param _drawAmount The amount of ETH to withdraw.
     */
    function withdraw(string memory _user, uint256 _drawAmount) public payable virtual{
        if(keccak256(abi.encodePacked(_user)) == keccak256(abi.encodePacked("owner")) && msg.sender != i_owner ) {
            revert("Storage__NotOwner");
        }
        require(s_tokens[_user] >= i_minDraw && _drawAmount >= i_minDraw && s_tokens[_user] >= _drawAmount, "Storage__NotEnoughTokens");

        payable(msg.sender).transfer(_drawAmount);
        s_tokens[_user] = s_tokens[_user] - _drawAmount;
    }

    /**
     * @dev Allows the owner of the contract to register a new user.
     * @param _user The user id of tne new user.
     */
    function register(string memory _user) public virtual{
        require(!_userExists(_user), "Storage__UserExists");
        s_users.push(_user);
        s_tokens[_user] = 0;
    }

    /**
     * @dev Allows the owner of the contract to transfer a specified amount to another user.
     * @param _user The useri id of the receiver.
     * @param _balance The amount to transfer.
     */
    function addBalanceToUser(string memory _user, uint256 _balance) public virtual onlyOwner{
        require(_userExists(_user), "Storage__UserNotFound");
        s_tokens["owner"] -= _balance;
        s_tokens[_user] += _balance;
    }

    /**
     * @dev Returns the token balances of all users.
     */
    function getAllTokens() public view virtual returns (uint256[] memory) {
        uint256[] memory tokens = new uint256[](s_users.length);
        for(uint i = 0; i < s_users.length; i++){
            tokens[i] = s_tokens[s_users[i]]/TOKEN_ETH_RATIO;
        }
        return tokens;
    }

    /**
     * @dev Helper function that changes the balance of a user.
     * @param _amount amount to change the balance by.
     * @param _user user to change the balance of.
     * @param  _sign if true, subtracts the amount from the user's balance. If false, adds the amount to the user's balance.
     */
    function changeBalance( uint256 _amount, string memory _user, bool _sign) internal {
        uint256 newBalance = _amount;
        if( _sign){
            require(s_tokens[_user] >= newBalance, "Storage__NotEnoughTokens");
            s_tokens[_user] -= newBalance;
            return;
        }
        s_tokens[_user] += newBalance;
    }

    /**
     * @dev Helper function that returns the index of a user in the s_users array.
     * @param _user The user id of the user to search for.
     */
    function _userExists(string memory _user) internal view returns (bool) {
        for (uint256 i = 0; i < s_users.length; i++) {
            if (keccak256(abi.encodePacked(s_users[i])) == keccak256(abi.encodePacked(_user))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @dev Returns the token balance of a user.
     * @param _user The user id of connected to the tokens.
     */
    function getTokens(string memory _user) public view virtual returns (uint256) {
        return s_tokens[_user]/TOKEN_ETH_RATIO;
    }

    function getMinDeposit() public view virtual returns (uint256) {
        return i_minDeposit;
    }

    function getMinDraw() public view virtual returns (uint256) {
        return i_minDraw;
    }
    function getOwner() public view virtual returns (address) {
        return i_owner;
    }
    function getUsers() internal view virtual returns (string[] memory) {
        return s_users;
    }
}