// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

// Importing the TransactionStruct library for handling transaction structures.
import "./library/TransactionStruct.sol";

// Custom error for unauthorized access attempts.
error TransactionStorage__NotOwner();

/**
 * @title Transaction Storage Contract
 * @dev Stores and manages financial transactions for users.
 *      Allows only the owner to add transactions.
 */
contract TransactionStorage {
    // Immutable owner address set at contract deployment.
    address private immutable owner;

    // Using the TransactionStruct library for Transaction data type.
    using TransactionStruct for TransactionStruct.Transaction;

    // Mapping from user IDs to their transaction arrays.
    mapping(string => TransactionStruct.Transaction[]) private transactions;

    /**
     * @dev Constructor sets the contract deployer as the owner.
     */
    constructor() {
        owner = msg.sender;
    }

    // Modifier to restrict function access to the contract owner only.
    modifier onlyOwner() {
        if (msg.sender != owner) {
            revert TransactionStorage__NotOwner();
        }
        _;
    }

    /**
     * @dev Adds a new transaction for a user. Only callable by the owner.
     * @param _userId The ID of the user for whom the transaction is recorded.
     * @param _transactionType The type of transaction (e.g., deposit, withdraw).
     * @param _amount The amount involved in the transaction.
     */
    function addTransaction(string memory _userId, string memory _transactionType, uint256 _amount) external onlyOwner {
        transactions[_userId].push(TransactionStruct.Transaction(_transactionType, _amount, block.timestamp));
    }

    /**
     * @dev Retrieves all transactions for a given user, in reverse order.
     * @param _userId The ID of the user whose transactions are to be retrieved.
     * @return An array of Transaction structures.
     */
    function getTransactions(string memory _userId) external view returns (TransactionStruct.Transaction[] memory) {
        TransactionStruct.Transaction[] memory userTransactions = transactions[_userId];
        uint256 length = userTransactions.length;

        TransactionStruct.Transaction[] memory reversedTransactions = new TransactionStruct.Transaction[](length);

        for (uint256 i = 0; i < length; i++) {
            reversedTransactions[length - 1 - i] = userTransactions[i];
        }

        return reversedTransactions;
    }

    function getOwner() external view returns (address) {
        return owner;
    }

}
