// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

// Custom errors for handling specific game conditions.
error GameContract__UserAlreadyInGame();
error GameContract__UserNotInGame();
error GameContract__GameIsFull();
error GameContract__GameIsFinished();
 
 /**
 * @title Game Contract
 * @dev Manages game-related activities such as participant management and game state.
 */
contract GameContract{
    /* Variables */
    uint256 private immutable i_buyIn;
    uint256 private s_pot;
    uint256 private immutable i_gameId;
    uint256 private immutable i_timeStamp;
    uint16 private immutable i_maxParticipants;
    bool private s_isFinished = false;

    /* Structurs */
    struct Participant {
        uint8 lastQuestion;
        bool isPlaying;
    }

    /* Arrays and other datastructurs */
    string[] private s_users;
    mapping (string => Participant) private s_participants;

    /* Functions */

    /**
     * @dev Sets up the game with specified parameters.
     * @param _buyIn Buy-in amount for the game.
     * @param _gameId Unique ID for the game.
     * @param _timeStamp Timestamp when the game was created.
     * @param _maxParticipants Maximum allowed participants in the game.
     */
    constructor(uint256 _buyIn, uint256 _gameId, uint256 _timeStamp, uint16 _maxParticipants) {
        i_buyIn = _buyIn;
        i_gameId = _gameId;
        s_pot = 0;
        i_timeStamp = _timeStamp;
        i_maxParticipants = _maxParticipants;
    }

    /**
     * @dev Adds a participant to the game.
     * @param _userId The ID of the user to add.
     */
    function addParticipant(string memory _userId) public{
        if(s_isFinished){
            revert GameContract__GameIsFinished();
        }
        if(s_users.length >= i_maxParticipants){
            revert GameContract__GameIsFull();
        }
        for(uint i = 0; i < s_users.length; i++){
            if(keccak256(abi.encodePacked(s_users[i])) == keccak256(abi.encodePacked(_userId))){
                revert GameContract__UserAlreadyInGame();
            }
        }
        s_users.push(_userId);
        s_participants[_userId] = Participant(0, true);
        s_pot += i_buyIn;
    }

    /**
     * @dev Retrieves the last question number answered by a participant.
     * @param _userId The ID of the user whose question count is requested.
     * @return The last question number answered by the user.
     */
    function getQuestionCounter(string memory _userId) public view returns(uint8){
        return s_participants[_userId].lastQuestion;
    }

    /**
     * @dev Calculates the average last question answered by all participants.
     * @return The average of the last questions answered across all participants.
     */
    function getAverageQuestionCounter() public view returns(uint8){
        uint8 sum = 0;
        uint8 count = 0;
        for (uint i = 0; i < s_users.length; i++) {
                sum += s_participants[s_users[i]].lastQuestion;
                count++;
        }
        if(count == 0){
            return 0;
        }
        return sum/count;
    }

    /**
     * @dev Determines the winners of the game based on certain criteria.
     * @return An array of user IDs who are winners.
     */
    function getWinners() public view returns(string[] memory){
        string[] memory winners = new string[](s_users.length);
        uint8 count = 0;
        for (uint i = 0; i < s_users.length; i++) {
            if(s_participants[s_users[i]].isPlaying && s_participants[s_users[i]].lastQuestion == 10){
                winners[count] = s_users[i];
                count++;
            }
        }
        return winners;
    }

    /**
     * @dev Checks if a user is participating in the game.
     * @param _userId The ID of the user to check.
     * @return True if the user is in the game, otherwise false.
     */
    function isInGame(string memory _userId) public view returns(bool){
        for(uint i = 0; i < s_users.length; i++){
            if(keccak256(abi.encodePacked(s_users[i])) == keccak256(abi.encodePacked(_userId))){
                return true;
            }
        }
        return false;
    }

    /**
     * @dev Marks the game as finished.
     */
    function finishGame() public{
        if (s_isFinished){
            revert GameContract__GameIsFinished();
        }
        s_isFinished = true;
    }

    /**
     * @dev Updates the last question answered by a participant.
     * @param _userId The ID of the participant.
     * @param _lastQuestion The last question number answered.
     */
    function updateParticipant(string memory _userId, uint8 _lastQuestion) public{
        if(s_isFinished){
            revert GameContract__GameIsFinished();
        }
        s_participants[_userId].lastQuestion = _lastQuestion;
        if(_lastQuestion < 10){
            s_participants[_userId].isPlaying = false;
        }
    }

    /**
     * @dev Determines if a user is a winner of the game.
     * @param _userId The ID of the user to check.
     * @return True if the user is a winner, otherwise false.
     */
    function isWinner(string memory _userId) public view returns(bool){
        if(s_participants[_userId].lastQuestion == 10){
            return true;
        }
        return false;
    }

    function getPot() public view returns(uint256){
        return s_pot;
    }

    function getGameId() public view returns(uint256){
        return i_gameId;
    }

    function getBuyIn() public view returns(uint256){
        return i_buyIn;
    }

    function getTimeStamp() public view returns(uint256){
        return i_timeStamp;
    }
} 