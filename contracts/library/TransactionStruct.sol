// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

/**
 * @title Transaction Structure Library
 * @dev Library to define and handle transaction-related data structures.
 *      Used in conjunction with contracts that handle financial transactions.
 */
library TransactionStruct {

    /**
     * @dev Structure representing a financial transaction.
     * @param transactionType Type of the transaction (e.g., deposit, withdraw).
     * @param amount The amount involved in the transaction.
     * @param timeStamp The timestamp when the transaction occurred.
     */
    struct Transaction {
    string transactionType;
    uint256 amount;
    uint256 timeStamp;
    }

}
