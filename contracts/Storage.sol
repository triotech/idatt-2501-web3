// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

// Importing external contracts and libraries.
import "./GameContract.sol";
import "./TransactionContract.sol";
import "./library/TransactionStruct.sol";
import "./StorageLogic.sol";

/**
 * @title Storage Contract
 * @dev Extends StorageLogic to manage game-related data and transactions
 */
contract Storage is StorageLogic {

    // Struct to store user scores.
    struct UserScore {
        string userName;
        uint256 score;
    }

    // Private variables to store transaction and game data.
    TransactionStorage private s_transactionStorage;
    GameContract[] private s_games;
    mapping (string => GameContract[]) private s_userToGame;

    /**
     * @dev Constructor to initialize the contract with minimum deposit and draw values.
     * @param _minDeposit Minimum deposit value.
     * @param _minDraw Minimum draw value.
     */
    constructor(uint256 _minDeposit, uint256 _minDraw) StorageLogic(_minDeposit, _minDraw) {
        s_transactionStorage = new TransactionStorage();
    }

    /* Functions */

    /**
     * @dev Allows a user to deposit money into the contract.
     * @param _user The username of the depositor.
     */
    function deposit(string memory _user) public payable override {
        super.deposit(_user);
        s_transactionStorage.addTransaction(_user, "deposit", msg.value);
    }

    /**
     * @dev Allows a user to withdraw a specified amount.
     * @param _user The username of the withdrawer.
     * @param drawAmount The amount to withdraw.
     */
    function withdraw(string memory _user, uint256 drawAmount) public payable override {
        super.withdraw(_user, drawAmount);
        s_transactionStorage.addTransaction(_user, "withdraw", drawAmount);
    }

    /**
     * @dev Allows the owner of the contract to transfer a specified amount to another user.
     * @param _user The useri id of the receiver.
     * @param _balance The amount to transfer.
     */
    function addBalanceToUser(string memory _user, uint256 _balance) public override {
        super.addBalanceToUser(_user, _balance);
        s_transactionStorage.addTransaction(_user, "addBalance", _balance);
    }

    /**
     * @dev Allows the owner of the contract to register a new user.
     * @param _user The user id of tne new user.
     */
    function register(string memory _user) public override {
        StorageLogic.register(_user);
    }

    /**
     * @dev Adds a new game to the contract.
     * @param _gameId the game id
     * @param _buyIn The buy-in amount for the game.
     * @param _timeStamp The timestamp when the game was created.
     * @param _players The maximum number of players allowed in the game.
     */
    function addGame(uint256 _gameId, uint256 _buyIn, uint256 _timeStamp, uint16 _players) public onlyOwner {
        require(!_gameExists(_gameId), "Storage__GameExists");
        s_games.push(new GameContract(_buyIn, _gameId, _timeStamp, _players));
    }

    /**
     * @dev Adds a participant to a game.
     * @param _gameId the game id
     * @param _userId the user id
     */
    function addParticipantToGame(uint256 _gameId, string memory _userId) public onlyOwner {
        require(_userExists(_userId), "Storage__UserNotFound");
        GameContract game = getGame(_gameId);
        StorageLogic.changeBalance(game.getBuyIn(), _userId, true);
        game.addParticipant(_userId);
        s_transactionStorage.addTransaction(_userId, "game", game.getBuyIn());
        s_userToGame[_userId].push(game);
    }

    /**
     * @dev Helper function to check if a game exists.
     * @param _gameId The ID of the game to check.
     */
    function _gameExists(uint256 _gameId) internal view returns (bool) {
        for (uint i = 0; i < s_games.length; i++) {
            if (s_games[i].getGameId() == _gameId) {
                return true;
            }
        }
        return false;
    }

    /**
     * @dev Deals the money to the winners of a game.
     * @param _gameId The ID of the game to deal the money for.
     */
    function dealMoneyToWinners(uint256 _gameId) public onlyOwner{
        require(_gameExists(_gameId), "Storage__GameNotFound");
        GameContract game = getGame(_gameId);
        string[] memory winners = game.getWinners();
                uint256 pot = game.getPot();
                uint256 counter = 0;
                for (uint j = 0; j < winners.length; j++) {
                    if(keccak256(abi.encodePacked(winners[j])) != keccak256(abi.encodePacked(""))){
                        counter++;
                    }
                }
                if(counter == 0){
                    StorageLogic.changeBalance(pot, "owner", false);
                    return;
                }
                uint256 potPerWinner = pot*80/100/counter;
                for (uint j = 0; j < winners.length; j++) {
                    StorageLogic.changeBalance(potPerWinner, winners[j], false);
                    s_transactionStorage.addTransaction(winners[j], "win", potPerWinner);
                }
    }
    
    /**
     * @dev Helper Function to get a game by ID.
     * @param _gameId The ID of the game to get.
     */
    function getGame(uint256 _gameId) internal view returns(GameContract){
        require(_gameExists(_gameId), "Storage__GameNotFound");
        for (uint i = 0; i < s_games.length; i++) {
            if(s_games[i].getGameId() == _gameId){
                return s_games[i];
            }
        }
    }

    /**
     * @dev Returns the avegrage correct answers of a user
     * @param _userId  The ID of the user to get.
     */
    function getAverageQuestionOfUser(string memory _userId) public view returns(uint256){
        uint256 sum = 0;
        uint256 count = 0;
        for (uint i = 0; i < s_games.length; i++) {
            if(s_games[i].isInGame(_userId)){
                sum += s_games[i].getQuestionCounter(_userId);
                count++;
            }
        }
        if(count == 0){
            return 0;
        }
        return sum*100/count;
    }

    /**
     * @dev Returns the avegrage correct answers of a game
     * @param _gameId  The ID of the game to get.
     */
    function getAverageQuestionOfGame(uint256 _gameId) public view returns(uint8){
        require(_gameExists(_gameId), "Storage__GameNotFound");
        GameContract game = getGame(_gameId);
        return game.getAverageQuestionCounter();
    }

    /**
     * @dev Returns all transactions of a user
     * @param _userId The ID of the user to get.
     */
    function getTransactions(string memory _userId) public view returns (TransactionStruct.Transaction[] memory) {
        return s_transactionStorage.getTransactions(_userId);
    }

    /**
     * @dev Return returns boolean if user is in game
     * @param _gameId The game id
     * @param _userId The user id
     */
    function isUserInGame(uint256 _gameId, string memory _userId) public view returns(bool){
        require(_gameExists(_gameId), "Storage__GameNotFound");
        GameContract game = getGame(_gameId);
        return game.isInGame(_userId);
    }

    /**
     * @dev Returns the total wins of a user
     * @param _userId The user id
     */
    function getTotalWins(string memory _userId) public view returns(uint256){
        uint256 sum = 0;
        for (uint i = 0; i < s_userToGame[_userId].length; i++) {
            if(s_userToGame[_userId][i].isWinner(_userId)){
                sum++;
            }
        }
        return sum;
    }

    /**
     * @dev Returns the score table of all users
     */
    function getScoreTable() external onlyOwner view  returns (UserScore[] memory){
        string[] memory users = StorageLogic.getUsers();

        UserScore[] memory scoreTable = new UserScore[](users.length);
        for (uint i = 0; i < users.length; i++) {
            scoreTable[i] = UserScore({
                userName: users[i],
                score: this.getTotalWins(users[i])
            });
        }
        return scoreTable;
    }

    /**
     * @dev Returns the info fo all users of a user
     * @param _userId The ID of the user to get.
     */
    function getUserGamesInfo(string memory _userId) external view returns (GameInfo[] memory) {
        GameInfo[] memory users = new GameInfo[](s_userToGame[_userId].length);
        for (uint i = 0; i < s_userToGame[_userId].length; i++) {
            users[i] = GameInfo({
                buyIn: s_userToGame[_userId][i].getBuyIn(),
                timeStamp: s_userToGame[_userId][i].getTimeStamp(),
                participantsNumber: s_userToGame[_userId][i].getPot() / s_userToGame[_userId][i].getBuyIn(),
                lastQuestion: s_userToGame[_userId][i].getQuestionCounter(_userId)
            });
        }
        return users;
    }

    /**
     * @dev Updates the last question of all users of a game
     * @param _gameId The game id
     * @param _user_ids An array of user ids
     * @param _lastQuestion An array of last questions
     */
    function updateGame(uint256 _gameId, string[] memory _user_ids, uint8[] memory _lastQuestion) external onlyOwner {
        require(_gameExists(_gameId), "Storage__GameNotFound");
        GameContract game = getGame(_gameId);
        for (uint i = 0; i < _user_ids.length; i++) {
            game.updateParticipant(_user_ids[i], _lastQuestion[i]);
        }
    }

    /**
     * @dev Structure representing information about a game.
     *      Used to store and retrieve game details.
     */
    struct GameInfo {
        uint256 buyIn;
        uint256 timeStamp;
        uint256 participantsNumber;
        uint256 lastQuestion;
    }

    function getTokens(string memory _userId) public view override returns (uint256) {
        return StorageLogic.getTokens(_userId);
    }

    function getMinDeposit() public view override returns (uint256) {
        return StorageLogic.getMinDeposit();
    }

    function getMinDraw() public view override returns (uint256) {
        return StorageLogic.getMinDraw();
    }

}