/**
 * Script for deploying the Storage contract.
 * Uses hardhat's deployments plugin for deployment logistics.
 *
 * @module Deploy script for Storage contract.
 *
 * @param {Object} getNamedAccounts - Hardhat's utility function to get named accounts from config.
 * @param {Object} deployments - Hardhat's deployments object for managing deployments.
 */
const { networkConfig, developmentChains } = require("../helper-hardhat-config")
require("dotenv").config()
const { network } = require("hardhat")
const { verify } = require("../utils/verify")

module.exports = async ({ getNamedAccounts, deployments }) => {
    const { deploy, log } = deployments
    const { deployer } = await getNamedAccounts()

    // Arguments for Storage contract constructor
    const args = [1e12, 1e14]

    // Deploying the Storage contract
    const storageContract = await deploy("Storage", {
        from: deployer,
        args: args,
        log: true,
        gas:5000000,
        gasPrice: 300235626043,
        waitConfirmations: network.config.blockConfirmations || 1,
    })

    // Verify the contract on Etherscan if it's not on a development chain
    if (
        !developmentChains.includes(network.name) &&
        process.env.ETHERSCAN_API_KEY
    ) {
        log("Verifying...")
        await verify(storageContract.address, args)
    }

    log("Storage deployed!")
    log("------------------------------------------------------------")
}

module.exports.tags = ["all", "storage"]
