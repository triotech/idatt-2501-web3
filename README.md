# TokenTrivia Web3 Project

## Table of contents

1. [Description](#description)
2. [Installation](#installation)
3. [Functionalities](#functionalities)
4. [Contracts](#contracts)
5. [Future work and possible changes](#future-work-and-possible-changes)
6. [Contribution](#contribution)
7. [License Information](#license-information)
8. [Contact information](#contact-information)

## Description
This repository contains a HardHat project that aims to create, test and deploy alle the smart contracts and libraries that compose the web application TokenTrivia. They are four contracts and one library. They are 'GameContract', 'Storage', 'StorageLogic', and 'TransactionContract'. The library is 'TransactionStruct'.

This project is connected to a EVM (Ethereum Virtual Machine) compatible test blockchain called [Sepolia](https://www.alchemy.com/overviews/sepolia-testnet). This connection to the Sepolia testnet allows developers and users to interact with the TokenTrivia contracts in a test environment, simulating real-world blockchain conditions without the need for real cryptocurrency. Utilizing Sepolia ensures a safe and cost-effective way to develop, test, and demonstrate the project's capabilities before deployment on the Ethereum mainnet. This approach is ideal for iterating and refining smart contracts and application features, ensuring robustness and reliability before launching in a live environment.

A direct connection to the blockchain cannot be established through the internet alone. Instead, it is essential to utilize a middleware service known as 'Nodes' to facilitate communication with the blockchain. For this project, we use a node provided by [alchemy](https://www.alchemy.com/), which offers additional useful services such as traffic analysis graphs and transaction logs. This setup ensures a reliable and efficient connection to the blockchain, allowing for seamless interaction with the network while also providing valuable insights into network activity and transaction history.

We extend our heartfelt thanks to the Sepolia organization for their invaluable support in providing the Sepolia testnet free of charge. This generosity has been instrumental in facilitating the development and testing of our project. Additionally, we are immensely grateful to the Alchemy team for their daily provision of 0.5 Sepolia ETH at no cost. This contribution is a significant enabler for our continuous development and experimentation on the testnet.


## Installation

To set up TokenTrivia web3 project on your local machine, follow these steps:

1. Clone the repository:

- Clone with SSH:
```bash
git clone git@gitlab.stud.idi.ntnu.no:triotech/idatt-2501-web3.git
```
- Clone with HTTPS:
```bash
git clone https://gitlab.stud.idi.ntnu.no/triotech/idatt-2501-web3.git
```

2. Install the dependencies:
```bash
npm install
```

3. Compile the contracts:
```bash
npm run compile
```

## Functionalities
As name before this project will be use to create, test and deploy the contracts. It is also possible to run the contracts locally for test support.

- **Create:** The contracts are code by using their own programming language, Solidity.
- **Test:** For the testing of the contracts, The projects uses the library "Chai" that is included when a Hardhat project is installed. In addition a package called 'Gas-Reporter' is implemented in the project. This will generate a document with the estimate the gass price of the tested function. The estimate price will be represented in USD and ETH. 

To test the contract, use the command:

```bash
npm run test:unit
```
- **Deploy:** To facilitate the deployment of contracts on the blockchain, the project employs the 'deploy' command, powered by Hardhat. This command takes advantage of the project's node to publish the contracts onto the blockchain. Specific contracts and their parameters for deployment are determined by JavaScript files located in the 'deploy' folder. These files are prefixed with numbers indicating the order of deployment, an essential feature for larger projects that might deploy multiple independent contracts. However, in this project's case, there is only one primary contract, simplifying the process.

To deploy the contract locally, use the command:
```bash
npm run deploy
```
To deploy the contract on the Sepolia tesnet, use the command:
```bash
npm run deploy:sepolia
```
It's crucial to understand that contract deployment should only occur after thorough testing. This is to ensure that potential system errors, which could negatively impact users, are minimized. Careful testing before deployment not only enhances system reliability but also helps avoid the complexities and additional efforts associated with deploying a new, revised contract on the blockchain.

## Contracts

- **StorageLogic:** The goal of this smart contract is to provide foundational logic and data storage functionality for the TokenTrivia application. It includes mechanisms for handling user registrations, deposits, withdrawals, and maintaining a balance system for the application's tokens. Key features include functions like **deposit**, **withdraw**, **register**, and **getTokens**, all designed to ensure secure and efficient handling of user interactions with the token system.

- **Storage:** This is the main contract of TokenTrivia and inherits the variables and functions of the StorageLogic Contract. It acts as a central hub, linking various aspects of the application together. Storage extends the capabilities of StorageLogic by integrating game-related functionalities, such as managing games, participants, and distributing rewards. Notable features include methods for adding games, handling user participation in games, and calculating user scores and statistics.

- **GameContract:** This contract is responsible for managing individual games within the TokenTrivia application. It handles the registration of participants, tracking of each participant's progress, and the calculation of winners. Key functionalities include the ability to add participants to the game, keep track of the game's pot, record the progress of each player, and determine the game's winners. It also includes features to check if a game is finished, manage participant updates, and identify winners.

- **TransactionStorage:** This contract is designed to store and manage transaction data within the TokenTrivia application. It utilizes the **TransactionStruct** library to structure transaction data. The contract includes functions to add new transactions, retrieve transaction history for a user, and identify the owner of the contract. It ensures that only the owner can add transactions, thereby maintaining the integrity and security of transaction data.

### Libraries
- **TransactionStruct:** This library is essential for the TokenTrivia ecosystem, providing a standardized format for handling transaction data. It enables other contracts to efficiently manage and record various transactions, ensuring that all transactional information is consistently structured and easily accessible.

## Future work and possible changes

The main change that is recommended is to move the keys and passwords currently stored in the '.env' file to the actual environment variables of the machine. This recommendation will enhance the security of the application by reducing the risk of sensitive data exposure, especially in cases where the '.env' file might accidentally be included in version control or be accessible in shared or public repositories. Storing keys and passwords as environment variables ensures that they are securely integrated into the system’s environment and are less susceptible to unauthorized access.

This project has intentionally placed the keys within a '.env' file to simplify the testing process for all potential users. This approach ensures that anyone interested in exploring or testing the application can do so without the need for their own wallet, Alchemy key, or other specific requirements. By centralizing these credentials in a '.env' file, we aim to lower the entry barrier for new users and developers, making it more accessible for community engagement and feedback. However, it's important to note that this method is primarily for testing purposes and should not be replicated in a production environment, where security and privacy are paramount.

If you're considering transitioning from the Sepolia blockchain to another EVM (Ethereum Virtual Machine) compatible blockchain, such as Ethereum mainnet or Polygon, our project is designed to support such flexibility. To make this switch, you'll need to add a new network configuration to the networkConfig constant in the hardhat-config.js file. This configuration should include the actual network ID of your chosen blockchain, its name, and a key that links to a node connected to the new blockchain. We recommend using 'Alchemy' for this purpose, due to its proven reliability and the advantages previously outlined. This change allows users to tailor the project to various blockchain environments, catering to diverse development and testing needs

## Contributions
## Contributions

To contribute to our project, please follow these steps:

1. Before making any changes, create a new issue that describes the contribution you plan to add. This will help to ensure that your changes align with our project goals and development roadmap. 
2. Once the issue has been approved, create a new branch for the issue, using a descriptive name that summarizes the changes you plan to make. 
3. When you have completed your changes, push the branch to our Git repository. This will trigger our pipeline to check your changes for any issues or errors. 
4. If the pipeline check is successful, submit a merge request to the main branch. Please include a detailed description of your changes and any relevant information that may help us review your request. 
5. After your changes have been merged, close the issue and delete the branch from the repository. This helps to keep our repository organized and maintainable.

## License Information

All the smart contracts developed for this project are released under the MIT License, a widely used and respected open-source license in the software community. This choice reflects our commitment to the principles of open development and the sharing of knowledge. The MIT License grants users permission to freely use, modify, distribute, and sublicense the code, while maintaining the necessary legal protections for the original authors. We believe that by adopting the MIT License, we are fostering a collaborative and innovative environment where developers and enthusiasts alike can contribute to and benefit from our work, while respecting the intellectual efforts and contributions of the original creators.

## Contact information

For questions, suggestions, or issues, please contact: 

### Pedro Cardona
- Email: pedropca@stud.ntnu.no