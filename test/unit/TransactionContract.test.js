const { expect } = require("chai")
const { ethers } = require("hardhat")

describe("TransactionStorage", function () {
    let transactionStorage
    let owner
    let client

    this.beforeEach(async function () {
        const TransactionStorage =
            await ethers.getContractFactory("TransactionStorage")
        transactionStorage = await TransactionStorage.deploy()
        ;[owner, client] = await ethers.getSigners()
    })

    describe("constructor", function () {
        it("Check owner is set correctly", async function () {
            const contractOwner = await transactionStorage.getOwner()
            expect(contractOwner).to.equal(owner.address)
        })
    })

    describe("addTransaction", function () {
        it("Check transaction is added by owner", async function () {
            await transactionStorage.addTransaction(
                "user1",
                "deposit",
                ethers.parseEther("0.1"),
            )
            const userTransactions =
                await transactionStorage.getTransactions("user1")
            expect(userTransactions.length).to.equal(1)
            expect(userTransactions[0].transactionType).to.equal("deposit")
            expect(userTransactions[0].amount).to.equal(
                ethers.parseEther("0.1"),
            )
        })

        it("Check transaction is not added by non-owner", async function () {
            try {
                await transactionStorage
                    .connect(client)
                    .addTransaction(
                        "user1",
                        "withdraw",
                        ethers.parseEther("0.2"),
                    )
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'TransactionStorage__NotOwner()'",
                )
            }
        })
    })

    describe("getTransactions", function () {
        it("Check transactions are retrieved correctly", async function () {
            await transactionStorage.addTransaction(
                "user1",
                "deposit",
                ethers.parseEther("0.1"),
            )
            await transactionStorage.addTransaction(
                "user1",
                "withdraw",
                ethers.parseEther("0.05"),
            )

            const userTransactions =
                await transactionStorage.getTransactions("user1")

            expect(userTransactions.length).to.equal(2)
            expect(userTransactions[0].transactionType).to.equal("withdraw")
            expect(userTransactions[0].amount).to.equal(
                ethers.parseEther("0.05"),
            )
            expect(userTransactions[1].transactionType).to.equal("deposit")
            expect(userTransactions[1].amount).to.equal(
                ethers.parseEther("0.1"),
            )
        })
    })
})
