const { expect } = require("chai")
const { ethers } = require("hardhat")

describe("GameContract", function () {
    let gameContract
    this.beforeEach(async function () {
        const GameContract = await ethers.getContractFactory("GameContract")
        gameContract = await GameContract.deploy(
            ethers.parseEther("0.1"),
            2,
            Date.now() + 1000 * 60 * 60 * 24 * 7,
            3,
        )
        await gameContract.addParticipant("participant1")
    })
    describe("createGame", function () {
        it("Check the pot is correct", async function () {
            let buyIn = await gameContract.getBuyIn()
            buyIn = BigInt(buyIn)
            expect(await gameContract.getPot()).to.equal(
                buyIn * BigInt(1),
            )
        })
    })
    describe("addParticipant", function () {
        it("Check user is added to participants", async function () {
            await gameContract.addParticipant("participant2")
            expect(await gameContract.isInGame("participant2")).to.equal(true)
        })
        it("Check user is not added to participants if already added", async function () {
            try {
                await gameContract.addParticipant("participant1")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'GameContract__UserAlreadyInGame()'",
                )
            }
        })
        it("Check user is not added to participants if game is full", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.addParticipant("participant3")
            try {
                await gameContract.addParticipant("participant4")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'GameContract__GameIsFull()'",
                )
            }
        })
        it("Check user is not added to participants if game is over", async function () {
            await gameContract.finishGame()

            try {
                await gameContract.addParticipant("participant5")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'GameContract__GameIsFinished()'",
                )
            }
        })
    })

    describe("finishGame", function () {
        it("Check game is finished", async function () {
            await gameContract.finishGame()
        })
        it("Check game is not finished if game is already finished", async function () {
            await gameContract.finishGame()
            try {
                await gameContract.finishGame()
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'GameContract__GameIsFinished()'",
                )
            }
        }
        )
    })

    describe("getPot", function () {
        it("Check pot is correct", async function () {
            let buyIn = await gameContract.getBuyIn()
            buyIn = BigInt(buyIn)
            expect(await gameContract.getPot()).to.equal(
                BigInt(1) * buyIn,
            )
            await gameContract.addParticipant("participant2")
            expect(await gameContract.getPot()).to.equal(
                BigInt(2) * buyIn,
            )
        })
    })

    describe("getBuyIn", function () {
        it("Check buy in is correct", async function () {
            expect(await gameContract.getBuyIn()).to.equal(
                ethers.parseEther("0.1"),
            )
        })
    })

    describe("isInGame", function () {
        it("Check user is in game", async function () {
            expect(await gameContract.isInGame("participant1")).to.equal(true)
        })
        it("Check user is not in game", async function () {
            expect(await gameContract.isInGame("participant2")).to.equal(false)
        })
    })

    describe("updateParticipant", function () {
        it("Check participant is updated", async function () {
            await gameContract.updateParticipant("participant1", 1)
            let winners = await gameContract.isWinner("participant1")
            expect(winners).false;
            await gameContract.updateParticipant("participant1", 10)
            winners = await gameContract.isWinner("participant1")
            expect(winners).true;
        })
        it("Check participant is not updated if game is finished", async function () {
            await gameContract.finishGame()
            try {
                await gameContract.updateParticipant("participant1", 1)
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with custom error 'GameContract__GameIsFinished()'",
                )
            }
        })
    })

    describe("getWinners", function () {
        it("Check winners are correct", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 1)
            await gameContract.updateParticipant("participant2", 10)
            const winners = await gameContract.getWinners()
            expect(winners.includes("participant2")).to.equal(true)
            expect(winners.includes("participant1")).to.equal(false)
        })
        it("Check winners are correct if multiple winners", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 10)
            await gameContract.updateParticipant("participant2", 10)
            const winners = await gameContract.getWinners()
            expect(winners.includes("participant2")).to.equal(true)
            expect(winners.includes("participant1")).to.equal(true)
            
        })
        it("Check winners are correct if no winners", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 1)
            await gameContract.updateParticipant("participant2", 1)
            const winners = await gameContract.getWinners()
            expect(winners.includes("participant2")).to.equal(false)
            expect(winners.includes("participant1")).to.equal(false)
            
        })
    })

    describe("getAverageQuestionCounter", function () {
        it("Check average question counter is correct", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 1)
            await gameContract.updateParticipant("participant2", 10)
            expect(await gameContract.getAverageQuestionCounter()).to.equal(5)
        })
        it("Check average question counter is correct if multiple winners", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 10)
            await gameContract.updateParticipant("participant2", 10)
            expect(await gameContract.getAverageQuestionCounter()).to.equal(10)
        })
        it("Check average question counter is correct if no winners", async function () {
            await gameContract.addParticipant("participant2")
            await gameContract.updateParticipant("participant1", 1)
            await gameContract.updateParticipant("participant2", 1)
            expect(await gameContract.getAverageQuestionCounter()).to.equal(1)
        })
    })
})
