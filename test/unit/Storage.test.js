const { expect } = require("chai")
const { ethers } = require("hardhat")

describe("Storage", function () {
    let storage
    let owner
    let client
    this.beforeEach(async function () {
        const Storage = await ethers.getContractFactory("Storage")
        storage = await Storage.deploy(
            ethers.parseEther("0.01"),
            ethers.parseEther("0.1"),
        )
        storage.register("user1")
        await storage.deposit("owner", { value: ethers.parseEther("10") })
        await storage.addBalanceToUser("user1", ethers.parseEther("0.5"))
        ;[owner, client] = await ethers.getSigners()
        await storage.addGame(
            1,
            ethers.parseEther("0.005"),
            Date.now() + 1000 * 60 * 60 * 24 * 7,
            10,
        )
    })
    describe("constructor", function () {
        it("Check initial values", async function () {
            const minDeposit = await storage.getMinDeposit()
            expect(minDeposit).to.equal(ethers.parseEther("0.01"))
            const minDraw = await storage.getMinDraw()
            expect(minDraw).to.equal(ethers.parseEther("0.1"))
        })
    })
    describe("register", function () {
        it("Check user is registered", async function () {
            await storage.register("user2")
        })
        it("Check user is not registered if already registered", async function () {
            try {
                await storage.register("user1")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__UserExists'",
                )
            }
        })
    })
    describe("addBalanceToUser", function () {
        it("Check user balance is added", async function () {
            await storage.addBalanceToUser("user1", ethers.parseEther("0.1"))
        })
        it("Check user balance is not added if not registered", async function () {
            try {
                await storage.addBalanceToUser(
                    "user2",
                    ethers.parseEther("0.1"),
                )
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__UserNotFound'",
                )
            }
        })
        it("Check user balance is not added if not owner", async function () {
            try {
                await storage
                    .connect(client)
                    .addBalanceToUser("user1", ethers.parseEther("0.1"))
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
    })
    describe("deposit", function () {
        it("Check user balance is added", async function () {
            await storage.deposit("user1", { value: ethers.parseEther("0.1") })
        })
        it("Check user balance is not added if not registered", async function () {
            try {
                await storage.deposit("user2", {
                    value: ethers.parseEther("0.1"),
                })
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
    })

    describe("withdraw", function () {
        it("Check user balance is withdrawn", async function () {
            await storage.withdraw("user1", ethers.parseEther("0.1"))
        })
        it("Check user balance is not withdrawn if not owner", async function () {
            try {
                await storage
                    .connect(client)
                    .withdraw("owner", ethers.parseEther("0.1"))
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
        it("Check user balance is not withdrawn if not enough balance", async function () {
            try {
                await storage.withdraw("user1", ethers.parseEther("2.2"))
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotEnoughTokens'",
                )
            }
        })
    })

    describe("getTokens", function () {
        it("Check user balance is withdrawn", async function () {
            const user1Balance = await storage.getTokens("user1")
            const ownerBalance = await storage.getTokens("owner")
            expect(user1Balance).to.equal(5000)
            expect(ownerBalance).to.equal(95000)
        })
    })

    describe("addGame", function () {
        it("Check game is added", async function () {
            await storage.addGame(
                2,
                ethers.parseEther("0.05"),
                Date.now() + 1000 * 60 * 60 * 24 * 7,
                10,
            )
        })
        it("Check game is not added if not owner", async function () {
            try {
                await storage
                    .connect(client)
                    .addGame(
                        2,
                        ethers.parseEther("0.05"),
                        Date.now() + 1000 * 60 * 60 * 24 * 7,
                        10,
                    )
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
        it("check if game is not added if game id already exists", async function () {
            try {
                await storage.addGame(
                    1,
                    ethers.parseEther("0.05"),
                    Date.now() + 1000 * 60 * 60 * 24 * 7,
                    10,
                )
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__GameExists'",
                )
            }
        })
    })

    describe("addParticipantToGame", function () {
        it("Check participant is added", async function () {
            await storage.addParticipantToGame(1, "user1")
            expect(await storage.isUserInGame(1, "user1")).to.equal(true)

        })
        it("Check participant is not added if not registered", async function () {
            try {
                await storage.addParticipantToGame(1, "user2")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__UserNotFound'",
                )
            }
        })
        it("Check participant is not added if not enough tokens", async function () {
            try {
                await storage.register("user3")
                await storage.addParticipantToGame(1, "user3")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotEnoughTokens'",
                )
            }
        })
        it("Check participant is not added if not owner", async function () {
            try {
                await storage.connect(client).addParticipantToGame(1, "user1")
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
    })


    describe("updateGame", function () {
        it("Check game is updated", async function () {
            await storage.addParticipantToGame(1, "user1")
            await storage.updateGame(1, ["user1"], [1])
            expect(await storage.getAverageQuestionOfUser("user1")).to.equal(100)
        })
    })

    describe("dealMoneyToWinners", function () {
        it("Check money is dealt to winners", async function () {
            await storage.register("user2")
            await storage.addBalanceToUser("user2", ethers.parseEther("0.5"))
            await storage.register("user3")
            await storage.addBalanceToUser("user3", ethers.parseEther("0.5"))
            await storage.addParticipantToGame(1, "user1")
            await storage.addParticipantToGame(1, "user2")
            await storage.addParticipantToGame(1, "user3")
            await storage.updateGame(1, ["user1", "user2", "user3"], [1, 2, 10])
            await storage.dealMoneyToWinners(1)
            expect(await storage.getTokens("user1")).to.equal(4950)
            expect(await storage.getTokens("user2")).to.equal(4950)
            expect(await storage.getTokens("user3")).to.equal(5070)
        })
        it("Check money is not dealt to winners if not owner", async function () {
            try {
                await storage.connect(client).dealMoneyToWinners(1)
            } catch (err) {
                expect(err.message).to.equal(
                    "VM Exception while processing transaction: reverted with reason string 'Storage__NotOwner'",
                )
            }
        })
        it("Check how much owner gets", async function () {
            await storage.register("user2")
            await storage.addBalanceToUser("user2", ethers.parseEther("0.5"))
            await storage.register("user3")
            await storage.addBalanceToUser("user3", ethers.parseEther("0.5"))
            await storage.addParticipantToGame(1, "user1")
            await storage.addParticipantToGame(1, "user2")
            await storage.addParticipantToGame(1, "user3")
            await storage.updateGame(1, ["user1", "user2", "user3"], [1, 2, 8])
            await storage.dealMoneyToWinners(1)
            expect(await storage.getTokens("owner")).to.equal(85150)
        })
    })

    describe("getAverageQuestionOfUser", function () {
        it("Check average question is returned", async function () {
            await storage.addParticipantToGame(1, "user1")
            expect(await storage.isUserInGame(1, "user1")).to.equal(true)
            await storage.updateGame(1, ["user1"], [1])
            const averageQuestion =
                await storage.getAverageQuestionOfUser("user1")
            expect(averageQuestion).to.equal(100)
        })
    })
    describe("getAverageQuestionOfGame", function () {
        it("Check average question is returned", async function () {
            await storage.addParticipantToGame(1, "user1")
            expect(await storage.isUserInGame(1, "user1")).to.equal(true)
            await storage.updateGame(1, ["user1"], [1])
            const averageQuestion = await storage.getAverageQuestionOfGame(1)
            expect(averageQuestion).to.equal(1)
        })
    })
    describe("getTransactions", function () {
        it("Check transactions are returned", async function () {
            await storage.deposit("user1", { value: ethers.parseEther("0.2") })
            await storage.withdraw("user1", ethers.parseEther("0.1"))
            const transactions = await storage.getTransactions("user1")
            expect(transactions.length).to.equal(3)
            expect(transactions[2].transactionType).to.equal("addBalance")
            expect(transactions[1].transactionType).to.equal("deposit")
            expect(transactions[0].transactionType).to.equal("withdraw")
            expect(Number(transactions[2].amount)).to.equal(500000000000000000)
            expect(Number(transactions[1].amount)).to.equal(200000000000000000)
            expect(Number(transactions[0].amount)).to.equal(100000000000000000)
        })
    })

    describe("getScoreTable", function () {
        it("Check score table is returned", async function () {
            await storage.register("user2")
            await storage.addBalanceToUser("user2", ethers.parseEther("0.5"))
            await storage.register("user3")
            await storage.addBalanceToUser("user3", ethers.parseEther("0.5"))
            await storage.addParticipantToGame(1, "user1")
            await storage.addParticipantToGame(1, "user2")
            await storage.addParticipantToGame(1, "user3")
            await storage.updateGame(1, ["user1", "user2", "user3"], [1, 2, 10])
            const scoreTable = await storage.getScoreTable()
            for (let i = 0; i < scoreTable.length; i++) {
                if(scoreTable[i].userName = "owner") continue;
                if(scoreTable[i].userName = "user3"){
                    expect(scoreTable[i].score).to.equal(BigInt(1))
                }else{
                    expect(scoreTable[i].score).to.equal(BigInt(0))
                }

        }}
        )
    });


})
